session-ledis [![Build Status](https://drone.io/gitea.com/tango/session-ledis/status.png)](https://drone.io/gitea.com/tango/session-ledis/latest) [![](http://gocover.io/_badge/gitea.com/tango/session-ledis)](http://gocover.io/gitea.com/tango/session-ledis)
======

Session-ledis is a store of [session](https://gitea.com/tango/session) middleware for [Tango](https://gitea.com/lunny/tango) stored session data via [ledis](http://ledisdb.io/). 

## Installation

    go get gitea.com/tango/session-ledis

## Simple Example

```Go
package main

import (
    "gitea.com/lunny/tango"
    "gitea.com/tango/session"
    "gitea.com/tango/session-ledis"
)

type SessionAction struct {
    session.Session
}

func (a *SessionAction) Get() string {
    a.Session.Set("test", "1")
    return a.Session.Get("test").(string)
}

func main() {
    o := tango.Classic()
    o.Use(session.New(session.Options{
        Store: redistore.New(ledistore.Options{
                Host:    "127.0.0.1",
                DbIndex: 0,
                MaxAge:  30 * time.Minute,
            }),
        }))
    o.Get("/", new(SessionAction))
}
```

## Getting Help

- [API Reference](https://pkg.go.dev/gitea.com/tango/session-ledis)
