module gitea.com/tango/session-ledis

go 1.15

require (
	gitea.com/lunny/log v0.0.0-20190322053110-01b5df579c4e
	gitea.com/lunny/tango v0.6.4
	gitea.com/tango/session v0.0.0-20201110080243-87f6e468e457
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible // indirect
	github.com/gomodule/redigo v1.8.2 // indirect
	github.com/siddontang/goredis v0.0.0-20180423163523-0b4019cbd7b7
	github.com/yuin/gopher-lua v0.0.0-20200816102855-ee81675732da // indirect
)
